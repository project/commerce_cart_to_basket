Converts "cart" to "basket".

Most strings are converted using string overrides, installed once when the module is enabled.

To remove the overrides, the module should be both disabled and uninstalled.

The path change from "http://www.example.com/cart" to "http://www.example.com/basket" takes
effect when the module is enabled. This change is removed when the module is disabled.